#include <iostream>
#include <portaudio.h>
#include <thread>

#include "Atomia.hpp"

void main_analyzer(int* run, QueueBuffer<std::vector<float>*>* sound_queue)
{
    std::vector<float>* data;
    while (*run)
    {
        data = sound_queue->pop();
        if (data != NULL) 
        {
            std::cout << sound_queue->size() << std::endl;
            delete data;
        }
        else std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
    
}

int main()
{
    Atomia atomia(main_analyzer);
    atomia.run();
    std::cout << "Recording started. Press Enter to stop." << std::endl;
    std::cin.ignore(); // Wait for user input
    atomia.stop();

    return 0;
}