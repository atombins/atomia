#ifndef QUEUE_BUFFER_HPP_INCLUDED
#define QUEUE_BUFFER_HPP_INCLUDED

#include <iostream>

enum QueueBufferError {QueueBufferError_EOK, QueueBufferError_Empty, QueueBufferError_Memory, QueueBufferError_Full};

typedef struct queue_element_t
{
    void* data;
    struct queue_element_t * next;
} queue_element;

template <typename T>
class QueueBuffer
{
private:
    int read_count = 0;
    int write_count = 0;
    int buffer_size_max;
    queue_element *read_head;
    queue_element *write_head;

public:
    QueueBuffer(size_t buffer_max_size, T initial_data);
    ~QueueBuffer();
    int push(T data);
    T pop();
    int size();
    T get_balance_element_data();
};

template <typename T>
QueueBuffer<T>::QueueBuffer(size_t buffer_max_size, T initial_data)
{
    this->buffer_size_max = buffer_max_size;
    queue_element* new_record = (queue_element*)malloc(sizeof(queue_element));
    if (new_record == NULL) throw std::runtime_error("Error during initializing QueueBuffer constructor.");
    new_record->next = NULL;
    new_record->data = (void*)initial_data;
    this->write_head = new_record;
    this->read_head = new_record;
}

template <typename T>
QueueBuffer<T>::~QueueBuffer()
{
    while (this->pop() != NULL);
    if (this->write_head != NULL) free(this->write_head);
}

template <typename T>
int QueueBuffer<T>::push(T data)
{
    if (this->size() >= this->buffer_size_max) return QueueBufferError_Full;
    queue_element* new_record = (queue_element*)malloc(sizeof(queue_element));
    if (new_record == NULL) return QueueBufferError_Memory;
    new_record->data = (void*)data;
    new_record->next = NULL;
    this->write_head->next = new_record;
    this->write_head = new_record;

    // It is now safe to read new_record.
    this->write_count++;
    return QueueBufferError_EOK;
}

template <typename T>
T QueueBuffer<T>::pop()
{
    if (this->read_count == this->write_count) return NULL;
    T data = (T)this->read_head->data;
    queue_element* to_free = this->read_head;
    this->read_head = to_free->next;
    free(to_free);

    // Element successfuly removed.
    this->read_count++;
    return data;
}

template <typename T>
T QueueBuffer<T>::get_balance_element_data()
{
    T data = (T)this->read_head->data;
    this->read_head = NULL;
    return data;
}

template <typename T>
int QueueBuffer<T>::size()
{
    return this->write_count - this->read_count;
}

#endif // QUEUE_BUFFER_HPP_INCLUDED