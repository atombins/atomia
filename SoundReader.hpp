#ifndef ASSISTANT_HPP_INCLUDED
#define ASSISTANT_HPP_INCLUDED

#include <iostream>
#include <portaudio.h>
#include <queue>

#include "QueueBuffer.hpp"

#ifndef SAMPLE_QUEUE_LIMIT
#define SAMPLE_QUEUE_LIMIT 100
#endif // SAMPLE_QUEUE_LIMIT
#ifndef SAMPLE_BLOCK_SIZE
#define SAMPLE_BLOCK_SIZE 4096
#endif // SAMPLE_BLOCK_SIZE

typedef struct stream_state_t
{
    QueueBuffer<std::vector<float>*> *sample_queue;
    std::vector<float> *sample;
} stream_state;

class SoundReader
{
private:
    PaStream* instream;
    stream_state _stream_state;
public:
    SoundReader();
    ~SoundReader();
    PaError start();
    void stop();
    QueueBuffer<std::vector<float>*>* get_stream() {return this->_stream_state.sample_queue;}
};

#endif // ASSISTANT_HPP_INCLUDED