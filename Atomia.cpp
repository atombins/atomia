#include "Atomia.hpp"

Atomia::Atomia(void (*main_analyzer)(int* run, QueueBuffer<std::vector<float>*>* sound_queue))
{
    this->main_analyzer = main_analyzer;
}

Atomia::~Atomia()
{
}

int Atomia::run()
{
    PaError error;
    error = this->sound_processor.start();
    if (error != paNoError)
    {
        std::cerr << "Error: " << Pa_GetErrorText(error) << std::endl;
        return 1;
    }
    this->is_running = 1;
    this->analyzer_thread = std::thread(this->main_analyzer, &this->is_running, this->sound_processor.get_stream());
    return 0;
}

void Atomia::stop()
{
    this->is_running = 0;
    this->analyzer_thread.join();
    this->sound_processor.stop();
}