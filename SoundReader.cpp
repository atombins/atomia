#include "SoundReader.hpp"

static int recordCallback(const void* inputBuffer, void* outputBuffer,
                          unsigned long framesPerBuffer,
                          const PaStreamCallbackTimeInfo* timeInfo,
                          PaStreamCallbackFlags statusFlags,
                          void* userData)
{
    const float* samples = (const float*)inputBuffer;
    stream_state *state = (stream_state*)userData;
    if (state->sample_queue->size() < SAMPLE_QUEUE_LIMIT)
    {
        for (unsigned int i = 0; i < framesPerBuffer; i++)
        {
            state->sample->push_back(samples[i]);
            if (state->sample->size() >= SAMPLE_BLOCK_SIZE)
            {
                state->sample_queue->push(state->sample);
                state->sample = new std::vector<float>();
            }
        }
    }

    return paContinue;
}

SoundReader::SoundReader()
{
    this->_stream_state.sample = new std::vector<float>();
    std::vector<float> *initial_sample = new std::vector<float>();
    for (int i = 0;i < SAMPLE_BLOCK_SIZE;i++) initial_sample->push_back(0.);
    this->_stream_state.sample_queue = new QueueBuffer<std::vector<float>*>(SAMPLE_QUEUE_LIMIT, initial_sample);
}

SoundReader::~SoundReader()
{
    std::vector<float>* to_free;
    while ((to_free = this->_stream_state.sample_queue->pop()) != NULL) delete to_free;
    delete this->_stream_state.sample_queue->get_balance_element_data();
    delete this->_stream_state.sample_queue;
    delete this->_stream_state.sample;
}

PaError SoundReader::start()
{
    PaError error;

    error = Pa_Initialize();
    if (error != paNoError)
    {
        return error;
    }

    error = Pa_OpenDefaultStream(&(this->instream), 1, 0, paFloat32, 44100, paFramesPerBufferUnspecified, recordCallback, (void*)&(this->_stream_state));
    if (error != paNoError)
    {
        Pa_Terminate();
        return error;
    }

    error = Pa_StartStream(this->instream);
    if (error != paNoError)
    {
        Pa_CloseStream(this->instream);
        Pa_Terminate();
        return error;
    }
    return paNoError;
}

void SoundReader::stop()
{
    (PaError)Pa_StopStream(this->instream);
    (PaError)Pa_CloseStream(this->instream);

    Pa_Terminate();
}