#ifndef ATOMIA_HPP_INCLUDED
#define ATOMIA_HPP_INCLUDED

#include <thread>

#include "SoundReader.hpp"

class Atomia
{
private:
    SoundReader sound_processor;
    int is_running = 0;
    std::thread analyzer_thread;
    void (*main_analyzer)(int* run, QueueBuffer<std::vector<float>*>* sound_queue);
public:
    Atomia(void (*main_analyzer)(int* run, QueueBuffer<std::vector<float>*>* sound_queue));
    ~Atomia();
    int run();
    void stop();
};

#endif // ATOMIA_HPP_INCLUDED